<?php
require_once("dbconfig.php");

$Nume = "";
$Prenume = "";
$Numecurs = "";
$Profesor ="";
$error = 0;
$error_text = "";


if(isset($_POST['Nume'])){
	$Nume = $_POST['Nume'];
}

if(isset($_POST['Prenume'])){
	$Prenume = $_POST['Prenume'];
}

if(isset($_POST['Numecurs'])){
	$Numecurs = $_POST['Numecurs'];
}

if(isset($_POST['Profesor'])){
	$Profesor = $_POST['Profesor'];
}


if(empty($Nume) || empty($Prenume) || empty($Numecurs) || empty($Profesor)){
	$error = 1;
	$error_text = "Unul dintre campuri este gol";
}

if(strlen($Nume) <= 3 || strlen($Prenume) <= 3){
	$error = 1;
	$error_text = "Numele sau prenumele este prea scurt";
}

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

    echo $response;
    return;

}

if($error == 0) {

    $stmt2 = $con -> prepare("INSERT INTO `studenti` (`Nume`, `Prenume`) VALUES(:Nume,:Prenume)");
    $stmt2 -> bindParam(':Nume',$Nume);
		$stmt2 -> bindParam(':Prenume',$Prenume);

							$stmt3 = $con -> prepare("INSERT INTO `cursuri` (`Numecurs`, `Profesor`) VALUES(:Numecurs,:Profesor)");
							$stmt3 -> bindParam(':Numecurs',$Numecurs);
							$stmt3 -> bindParam(':Profesor',$Profesor);

		if(!$stmt2->execute() || !$stmt3->execute()){

        $errors['connection'] = "Database Error";
    }

    else{
        echo "Success!";
    }
}else {
     echo $error_text;
     return;

}
