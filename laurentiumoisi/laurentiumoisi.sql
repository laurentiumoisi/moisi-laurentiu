-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Gazdă: 127.0.0.1
-- Timp de generare: dec. 18, 2018 la 05:07 PM
-- Versiune server: 10.1.36-MariaDB
-- Versiune PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Bază de date: `laurentiumoisi`
--

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `cursuri`
--

CREATE TABLE `cursuri` (
  `CursID` int(11) NOT NULL,
  `Numecurs` varchar(255) NOT NULL,
  `Profesor` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Eliminarea datelor din tabel `cursuri`
--

INSERT INTO `cursuri` (`CursID`, `Numecurs`, `Profesor`) VALUES
(1, 'Fizica', 'Isaac Newton'),
(2, 'Programare', 'Steve Jobs'),
(3, 'Cercetare', 'Albert Einstein'),
(4, 'Desen', 'Salvador Dali');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `prezenta`
--

CREATE TABLE `prezenta` (
  `StudentID` int(11) NOT NULL,
  `CursID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `studenti`
--

CREATE TABLE `studenti` (
  `StudentID` int(11) NOT NULL,
  `Nume` varchar(255) NOT NULL,
  `Prenume` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Eliminarea datelor din tabel `studenti`
--

INSERT INTO `studenti` (`StudentID`, `Nume`, `Prenume`) VALUES
(1, 'Popescu', 'Rares'),
(2, 'Munteanu', 'Costel'),
(3, 'Popa', 'Andreea'),
(4, 'Munteanu', 'Miruna');

--
-- Indexuri pentru tabele eliminate
--

--
-- Indexuri pentru tabele `cursuri`
--
ALTER TABLE `cursuri`
  ADD PRIMARY KEY (`CursID`);

--
-- Indexuri pentru tabele `prezenta`
--
ALTER TABLE `prezenta`
  ADD KEY `StudentID` (`StudentID`),
  ADD KEY `CursID` (`CursID`);

--
-- Indexuri pentru tabele `studenti`
--
ALTER TABLE `studenti`
  ADD PRIMARY KEY (`StudentID`);

--
-- AUTO_INCREMENT pentru tabele eliminate
--

--
-- AUTO_INCREMENT pentru tabele `cursuri`
--
ALTER TABLE `cursuri`
  MODIFY `CursID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pentru tabele `studenti`
--
ALTER TABLE `studenti`
  MODIFY `StudentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constrângeri pentru tabele eliminate
--

--
-- Constrângeri pentru tabele `prezenta`
--
ALTER TABLE `prezenta`
  ADD CONSTRAINT `prezenta_ibfk_1` FOREIGN KEY (`CursID`) REFERENCES `cursuri` (`CursID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
