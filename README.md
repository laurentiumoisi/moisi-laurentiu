Tabelul "Studenti" contine ordinea studentilor (StudentID-care este unic)
In cadrul acestui tabel avem informatii legate de studenti precum "nume/prenume"

In tabelul "Cursuri" contine, ca si tabelul "Studenti" un ID al cursurilor care este unic.
Legatura (check-in-ul) dintre cele doua tabele se face cu ajutorul tabelului "Prezenta" care primeste date de la cele doua tabele.
Folosim ID unic si foreign key pentru a introduce datele intr un singur tabel cat mai eficient.